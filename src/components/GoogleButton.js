import Button from './Button.style';
import React from 'react'
import { FcGoogle } from 'react-icons/fc'

export default function GoogleButton({name, onClick}) {
    return (
       <Button onClick={onClick}>
          <FcGoogle/>{name}
        </Button>
    )
}
