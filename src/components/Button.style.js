import styled from 'styled-components'

const Button = styled.button`
 border: none;
 padding: 10px;
 white-space: nowrap;
 display: flex;
 justify-content: space-evenly;
 flex-direction: row;
 width: 100px;
 border-radius:5px;
 font-size: 13px;
 outline: none;
  
 :hover {
    cursor: pointer;
    background-color: rgb(235,235,235);
 }
`

export default Button
