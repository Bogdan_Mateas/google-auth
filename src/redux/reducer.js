import { LOGIN } from "./actionTypes";

const initialState = {
  user: null
};

export default function(state = initialState, action) {
  switch (action.type) {
    case LOGIN:{
      return {
        ...state,
        user: action.payload.payload
      }
    }
    default:
      return state;
  }
}
