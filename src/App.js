import React, { Component } from 'react'
import Home from './containers/Home';
import About from './containers/MyAccount';
import {  Route, Switch } from "react-router-dom";
import "./App.css"
 class App extends Component {
  render() {
    return (

  <Switch>
      <Route path="/" component={Home} exact />
      <Route path="/my-account" component={About} />
  </Switch>
    )
  }
}



export default App;
