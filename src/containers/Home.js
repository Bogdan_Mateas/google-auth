import 'react-json-pretty/themes/monikai.css';
import React from 'react'
import { connect } from "react-redux";
import { login } from "../redux/actions";
import GoogleButton from '../components/GoogleButton';
import HomeWrapper from './HomeWrapper.style';
import { MESSAGES } from '../constants';

 const Home = ({login, history}) => {
    
  const jwtDecode = (t) => {
      let token = {};
      token.raw = t;
      token.header = JSON.parse(window.atob(t.split('.')[0]));
      token.payload = JSON.parse(window.atob(t.split('.')[1]));
      return (token)
    }
    

  const callback = (decodedJwt,token) => {
      login(decodedJwt)
      localStorage.setItem('token', token)
      history.push('/my-account')
    }

  const handleLogin = () =>  {
    const google = window.google;   
    google.accounts.id.initialize({
      client_id: '726027060085-u2nvdfdjhspfl14e7itqfjdtsh8rgc82.apps.googleusercontent.com',
      callback: (result)=> callback(jwtDecode(result.credential), result.credential)
    });
    google.accounts.id.prompt((notification) => {
      if (notification.isNotDisplayed() || notification.isSkippedMoment()) {
          // try next provider if OneTap is not displayed or skipped
      }
  });
  }

  
    return (  
    <HomeWrapper>  
      <div>{MESSAGES.WELCOME}</div>
      <GoogleButton name={MESSAGES.LOGIN} onClick={handleLogin}/>    
    </HomeWrapper>
    )
  }



const mapDipatchToProps = dispatch => ({
   login:(token)=>dispatch(login(token))
  })

export default connect(null, mapDipatchToProps)(Home);
