import styled from 'styled-components'

const MyAccountWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  width: 350px;
  height: 400px;
  border-radius: 6px;
  padding: 24px;
  font-size: 14px;
  margin-top: 20px;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  background-color:white;
  cursor:pointer;
 
 &:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
 }

 img {
  border-radius: 50%;
  width: 115px;
  height: 115px;
 }

 button {
  margin-top: 3em;
 }
`

export default MyAccountWrapper 
