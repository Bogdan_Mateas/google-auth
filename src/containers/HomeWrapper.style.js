import styled from 'styled-components'

const HomeWrapper = styled.div`
  width: 300px;
  height: 150px;
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  background-color: white;
`

export default HomeWrapper