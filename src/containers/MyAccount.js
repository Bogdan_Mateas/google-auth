import React, {  Fragment, useEffect } from 'react'
import { connect } from 'react-redux';
import { Avatar } from 'antd';
import JSONPretty from 'react-json-pretty'
import 'react-json-pretty/themes/monikai.css';
import monkai from 'react-json-pretty/dist/monikai'
import GoogleButton from '../components/GoogleButton'
import MyAccountWrapper from './MyAccountWrapper.style';
import { LABELS, MESSAGES } from '../constants';

const MyAccount = ({user, history}) => { 
   
    useEffect(() => {       
        if(!user) logout()
    }, [])

    const logout = () => {
        localStorage.removeItem('token')
        history.push('/')
    }

    if(!user) return null
    const { picture, email, name, hd } = user

    return (
        <Fragment>
            <MyAccountWrapper>                
                <div>
                    <Avatar src={ picture } />
                </div>
                <p>{LABELS.EMAIL}: { email || ''}</p>
                <p>{LABELS.NAME}: { name || ''}</p>
                <p>{LABELS.COMPANY}: { hd || `${LABELS.PERSONAL_ACCOUNT}`}</p>
                <GoogleButton name={MESSAGES.LOGOUT} onClick={() => logout()}/>
            </MyAccountWrapper>                
            <JSONPretty data={user} theme={monkai}/>               
        </Fragment>
        )
    }




const mapStateToProps = state => {
    return { user: state.user} 
  };


export default connect(mapStateToProps, null)(MyAccount);