export const LABELS = {
   EMAIL: 'email',
   NAME: 'name',
   COMPANY: 'company'
  }
export const MESSAGES = {
  WELCOME: 'Welcome!',
  LOGIN: 'Login',
  LOGOUT: 'Logout',
  PERSONAL_ACCOUNT: 'Personal account'
  }